@post-title{A fear of judgment}

I wanted to discuss a fear of mine.

@p{@a[#:href "https://www.youtube.com/watch?v=ZXu6q-6JKjA"]{https://www.youtube.com/watch?v=ZXu6q-6JKjA}}

When someone asks me where I'm from, I will have to say India, or depending on how I feel, the United Arab Emirates. But I will be in fear. Fear of what the person will utilize that fact for. Assume that I'm an 'immigrant'. Assume that I'm a 'foreigner'. Take on a different style of interaction.

Yes, it is likely that I will have nothing in common. I barely have anything in common with almost all the people I meet. When I talk to an Indian or an Emirati (citizen of the UAE), I don't have anything in common. My race and the location I've lived in say nothing about me. I identify with neither, and I'd like to think that I have a unique perspective as a third culture (or as I like to call myself, no culture) kid.

I am defined by beliefs, my curiosity, and my personality. Sure, my experience played a role in its development, but the prospect that people will take their limited knowledge of how varied an experience can be and use it to judge worries me. It worries me a lot.

I pick and choose. I am unique just like you. Give me a chance to express myself.