@post-title{Auditory medicine}

When in moments of emotional pain, I find myself going to many different types of music. I want to discuss those types, and try to understand why I do so.

@p{@a[#:href "https://www.youtube.com/watch?v=wjEOuyfv_dg"]{Autechre - Flutter}}

This song always ends up being the first song I listen to. It's weird, because it isn't even a sad song. It's hopeful. It's happy. Not @em{joyful}, happy. Atmospheric. Artificial. I don't think I hear a single instrument (excluding electronic sounds of course). To me, I think this provides an escape into a much better world. The hollow reverb gently transports me there, and the stimulating and variable percussion distracts me from thinking about anything else other than how cool the song is.

@p{@a[#:href "https://www.youtube.com/watch?v=A7ZxRs45tTg"]{Bicep - Glue}}

This song has only entered my library recently. I got the album it's a part of the day it came out. It was quite the magical experience. It shares a lot with the previous song, but sheds the happiness for a tinge of melancholy. It seems to say that things aren't good, they just are. And that state of being is beautiful. The mere fact that a thing is able to @em{be}.

@p{@a[#:href "https://www.youtube.com/watch?v=CMV4dl-x0Tc"]{Boards of Canada - Farewell Fire}}

Well, if there are tears involved, this song is typically the trigger. A complete depersonalizing experience. A musical dose of LSD I guess. I'm not really sure if that's what LSD does. Anyways, this song is great to stare at window or a ceiling with. The echo. The pauses. The minimalism. Ahh. Nothing really matters. But everything is great.

@p{@a[#:href "https://www.youtube.com/watch?v=U-om6PSZ94s"]{James Blake - Noise Above Our Heads}}

Now here's a song which the average music consumer might like. I really like the easy going but deep mood it creates. I just let the bass take control. I feel like this song makes me feel better just by the surface level aesthetics. I love the sample, and the choices the artist makes are unique and creative. Ah, this song is amazing.

@p{@a[#:href "https://www.youtube.com/watch?v=bJzB3_p0WTw"]{Forest Swords - Knife's Edge}}

This song provides a feeling of grandiosity. Deep down, I know that all the problems I'm facing are meaningless and that there are billion people out there (probably more) who face worse everyday. However, the temporary feeling like I'm at the center of the world and that there's a movie soundtrack backing my life is pretty cool. It's a great temporary feeling, before I go back to coping with the fact that my pain is just nothing and probably should be brushed off.

@p{@a[#:href "https://www.youtube.com/watch?v=vWwanrqCQ8E"]{Mono - Black Woods}}

This song is the perfect choice when anger finally comes to the surface. The amazing crescendo and the noise is just what I need to calm down and be the person that I want to be.

@p{@a[#:href "https://www.youtube.com/watch?v=nUL9Nerbk-s"]{Orbital - Dwr Budr}}

The pain I'm facing is just a temporary phase. Life will go on. There are people who persevere much more in order to achieve far simpler and easier goals. What I need and want are not important, it's what the world does. Sometimes that takes a sacrifice. The solution to all this anger and sadness is just that, a sacrifice. Of time, of personal happiness, of vices, of my human flaws. To just devote myself to the @a[#:href "/2018/01/28/on-goals/"]{one true goal}.

And then I'm not sad anymore, cause I've got stuff to get done and a goal to get back to. Overall, I think this is a pretty healthy way of dealing with extreme sadness. All it takes is a day with music listening device, which almost everybody in the world can have access to so I hope it's not too privileged. I am privileged to have the free time in order to devote an entire day to recovery, but oh well. No monetary cost, no harm to relationships, and mainly, no expressed anger.

And good music. Can't forget the amazing music. It's kinda cool how sounds with almost no words have such an effect on me.