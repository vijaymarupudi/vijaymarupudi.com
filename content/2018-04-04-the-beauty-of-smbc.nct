@post-title{The beauty of SMBC}

There are very few websites online that I follow with dedication, and @em{Saturday Morning Breakfast Comics} is one of them. A lot of the humor in those comics is very tongue in check and light-hearted, in a deep way. They are comics which make you feel better after reading them, and then make you think. That's not to say that they're all happy. The dark ones simply make you realize how absurd the world is and frame it in a healthy way.

Doing all of that is very hard work. Zach Weinersmith, the author, seems to spend a lot of time and effort in writing the jokes behind the comics because the majority of them seem to have a certain intellectual theme to them, and making something like that funny is not easy. Sometimes the comedy in the comics isn't a punchline per se, but brings to attention something that isn't thought about very often. The fact that the comics are presented with self-awareness and a lack of pretentiousness adds to the positive experience.

It does have the usual sex jokes, but there's a level of absurdity to them that they're somehow different and not the usual cheap jokes. Whenever Zach makes a cheap joke, he usually admits it in the bonus panel, which is sometimes funnier than the comic itself.

I'd like to show you some great examples. Note that I'm just pressing random on the website and typing out the good ones.

@ul{@li{@p{@a[#:href "https://www.smbc-comics.com/comic/2010-07-02"]{https://www.smbc-comics.com/comic/2010-07-02}}}

@li{@p{@a[#:href "https://www.smbc-comics.com/comic/the-psychology-of-heaven"]{https://www.smbc-comics.com/comic/the-psychology-of-heaven}}}

@li{@p{@a[#:href "https://www.smbc-comics.com/comic/2012-12-14"]{https://www.smbc-comics.com/comic/2012-12-14}}}

@li{@p{@a[#:href "https://www.smbc-comics.com/comic/planned-economics"]{https://www.smbc-comics.com/comic/planned-economics}}}

@li{@p{@a[#:href "https://www.smbc-comics.com/comic/2012-07-17"]{https://www.smbc-comics.com/comic/2012-07-17}}}

@li{@p{@a[#:href "https://www.smbc-comics.com/comic/2012-09-02"]{https://www.smbc-comics.com/comic/2012-09-02}}}

@li{@p{@a[#:href "https://www.smbc-comics.com/comic/a-physicist-chemist-and-an-economist-are-on-a-train"]{https://www.smbc-comics.com/comic/a-physicist-chemist-and-an-economist-are-on-a-train}}}

@li{@p{@a[#:href "https://www.smbc-comics.com/comic/2009-12-14"]{https://www.smbc-comics.com/comic/2009-12-14}}}

@li{@p{@a[#:href "https://www.smbc-comics.com/comic/one-wish-3"]{https://www.smbc-comics.com/comic/one-wish-3}}}

@li{@p{@a[#:href "https://www.smbc-comics.com/comic/2010-02-11"]{https://www.smbc-comics.com/comic/2010-02-11}}}

@li{@p{@a[#:href "https://www.smbc-comics.com/comic/i-am-no-longer-a-child"]{https://www.smbc-comics.com/comic/i-am-no-longer-a-child}}}

@li{@p{@a[#:href "https://www.smbc-comics.com/comic/2007-04-02"]{https://www.smbc-comics.com/comic/2007-04-02}}}

@li{@p{@a[#:href "https://www.smbc-comics.com/comic/2014-12-10"]{https://www.smbc-comics.com/comic/2014-12-10}}}

@li{@p{@a[#:href "https://www.smbc-comics.com/comic/2011-11-16"]{https://www.smbc-comics.com/comic/2011-11-16}}}

@li{@p{@a[#:href "https://www.smbc-comics.com/comic/wholesale"]{https://www.smbc-comics.com/comic/wholesale}}}

@li{@p{@a[#:href "https://www.smbc-comics.com/comic/2013-08-24"]{https://www.smbc-comics.com/comic/2013-08-24}}}

@li{@p{@a[#:href "https://www.smbc-comics.com/comic/2014-12-25-2"]{https://www.smbc-comics.com/comic/2014-12-25-2}}}}

If you look carefully at most of the comics, you'll realize that there's one thing Zach does that's unmatched by any other art maker that I'm aware of. He makes the typically oppressed people in society feel normal. You might not feel that way when you're first exposed to these comics, but Zach just casually features female scientists, black engineers, homosexual couples, and a lot more in the comics. While that might seem normal to some people (and some people might find it weird that I mention it), the focus of the comics is typically not about the skin color, gender identity, or the sexuality of the characters; It's about something else entirely. Modern media seems to love to announce to people when they're being progressive and doing the right thing. "Come watch this movie about a black superhero (backed by a hiphop sountrack)!" Instead the characters in the comics do nothing that the stereotypes suggest and there's no mention of the "culture" they are supposed to follow. I'm a big fan of the gender role reversal comics.

The fact that he does it so naturally and effortlessly is brilliant. I feel racist, sexist, just cause I notice these things. I'd love to live in the society where the characters reside. Thank you Zach!