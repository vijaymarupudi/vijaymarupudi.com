@post-title{Reflections of 2020-2021, a review of this website, and what's to come}

A global pandemic had occurred, and there have been crickets on this
website. Here's what happened.

@a[#:href "https://www.youtube.com/watch?v=J00IWVtbXvQ"]{Low - HEY WHAT}

On the 31st of December, 2021, I wrote a blog post. I wrote it while I
was in the UAE, perhaps for my last time in my life, as my parents
lost their jobs over the pandemic. I was tired of the world's
pessimism and wanted to outline my positive impression of very
inspiring activist.

Then stuff got busy. It was 2020, and I had to go to India and
navigate the intricate social network that was my family. Lots of
visiting people I barely knew, and a couple of reunions with people I
wanted to meet. People had changed. Yet they claimed to be the same.
Same old, same old.

Then came the second half of January. I went to a live musical
performance by the Delvon Lamaar Organ Trio in Minneapolis with
friends. This event was organized in a very ad-hoc way that I would
not have imagined even an year before 2020. I was nervous. It was fun!
The audience however kept asking for encores... I don't know, I can
only listen to anything for so long. The trio kept me there for a lot
longer than that. Nevertheless, it was fun. There was a certain
atmosphere that I don't typically find in my headphones.

January, you see, is a month with academic conference submissions. The
Cognitive Science 2020 conference deadlines were coming up. So were
the Mathematical Cognition and Learning Society Conference deadlines.
For someone who had submitted very few things to conferences, even an
abstract took a lot of learning and effort.

February was quite fun. I was learning about memory at 9pm during -20F
/ 0C nights. I was listening to new music. I was getting some
exercise. I was learning how to cook. I was playing badminton with
friends. I got to hang out with a flower in the white tundra called
Minnesota. I thought, wow, life is nice.

Then came March of 2020. It was the beginning of a blur. I'd been
keeping track of the news, of a pathogen now called COVID. It was in
Europe now. It was only about time it hit the USA. Soon enough, there
were headlines of COVID being in the USA. But life went on.

Until it didn't. There were empty grocery stores. There was a lot of
panic buying. Hordes of people bought life supplies of toilet paper. I
struggled to find food at Trader Joes. Never had I imagined seeing
empty grocery stores in the USA. And then came uncertainty. And then
came a great fuzz, a blurring of events to come. The resolution of my
memory drops greatly here.

It was interesting to observe people get accustomed to a new normal.
The world had to create new categories to make sense of it. People
could now be described by their acceptance of scientific institutions,
their tolerance for risk, their consideration of others around them,
and wider society as a whole. People's beliefs were visible in their
actions. I couldn't help but glare at the undergraduates who threw
parties during that time.

The world had to navigate an online time. However, I think this
largely resulted in an appreciation for in-person interaction. There
was constant discussion of latency and seeing yourself on the screen.
However, I felt the social side of me dying. I was never one to engage
in social media and preferred in-person interaction. However, that was
not an option now. Oh well. On the other hand, I started walking. A
lot.

Yet, I didn't become the reclusive monk most people that I expected
myself to become. My roommates had formed a fun community. I
had a flower in my room. It was okay!

Then came the time to part ways. I had to move to Georgia Tech. A move
during a pandemic was fun... Then I came right back to Minneapolis. It
was fun! I tried bouldering, and I liked it.

I was exposed to a whole new intellectual world. A world of tech
people discussing humanities and psychology. It was interesting to
learn and negotiate by existing views and beliefs with these new
colleagues. I now reside in a department where critical race theory
and reinforcement learning are both taught. Cool.

And then came 2022. Wait, 2021? What happened during 2021...? Let's
see.

@ul[
  @li{I visited the Georgia Aquarium.}
  @li{I had some food at Waffle House.}
  @li{Had pizza with my advisor every week and talked shop.}
  @li{I had fun trips to waterfalls with my lab.}
  @li{I TAed for a course with Sashank, that was fun.}
  @li{Switched from Vim to Emacs.}
  @li{Started using Nim and Guile Scheme.}
  @li{Switched to the Colemak-DH keyboard layout.}
]

Aannnd there! Now we're here. Phew! Now time seems to be slowing down.
And I can finally write in this blog.

But I'm now a different person. Is this even my blog? Do I even want
this content online now? I'm supposed to be a professional now. Some
is this content was written in anger, dated from a time when some
would consider me a child. Most of the content was not proof read. I
cringe at the typos and grammatical errors I find. Time to delete and
start fresh right? That'll maximize my job prospects in the future.

I think I won't do that. I'd grown a lot as a person over the years
and I don't want to act like growth didn't occur. I am constantly
learning and @a[#:href
"/blog/2019-10-19-faith-in-strangers.html"]{changing}. I have a more
nuanced view of the world now compared to even 2 years ago. I
appreciate more things. I have new skills. I've stopped caring about
things I've cared about. And I've had false beliefs. And I want others
to know that it's okay for them to admit these things too.

So sure, let employers and professors read this blog. I'm curious
about what they think. I'm excited to discuss and talk about the human
aspects whatever we do, instead of just the technical. And this blog
serves to me and the world as a reminder of the human behind the
name/face.

So what will this blog be about now? More of the same? Sure, but more
I'd say.

I've always loved writing, but the past few years had taught me the
psychological value of writing. Writing is hard, and writing well is a
superpower. Diction, style, grammar are just some parts of writing.
Writing is synonymous with thinking. Those who think well should be
able to write well. Writing allows your thoughts to become
immortalized in time, available to a new generation. And you can get
better by spending more time on it. A superpower you can learn. And I
want to get better at it.

I have more to offer to the world than self-reflections and opinions
about the state of the world, and I think I should write about it. I
might take a stab at blogging about things I'm learning, for both my
hobbies and work. I might explain things I already know well to those
who come after me. I might discuss my explorations of new concepts and
ideas. I might just complain about my life. I'm curious to see how
it'll turn out.
