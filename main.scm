(use-modules (ice-9 textual-ports)
             (ice-9 match)
             (ice-9 string-fun)
             (nectar)
             (nectar renderers html)
             (ice-9 ftw)
             (website music-page)
             (website home-page)
             (website blog-page)
             (website fonts-page)
             (website cv)
             (website atom))

(define BASE (call-with-input-file "templates/base.nct"
               get-string-all))

(define (paths . args)
  (string-join
   args
   "/"))

(define BUILD_DIR "build")
(define BLOG_DIR (paths BUILD_DIR "blog"))
(define STATIC_DIR "static")
(define CONTENT_DIR "content")
(define CACHE_DIR "cache")

(define-syntax-rule (dbind pattern expr . body)
  (match expr (pattern . body)))

(define (write-build-file filename string)
  (let ((filename (string-append BUILD_DIR "/" filename)))
    (call-with-output-file
        filename
      (lambda (port)
        (put-string port string)))))


(define* (render-base main
                      #:key
                      title
                      (description "Vijay's website")
                      (head ""))
  (let ((title (if title
                   (string-append title " | Vijay Marupudi")
                   "Vijay Marupudi")))
    (let loop ((str BASE)
               (reps `(("X_TITLE_X" . ,(nectar-html-escape-string title))
                       ("X_DESCRIPTION_X" . ,(nectar-html-escape-string description))
                       ("X_HEAD_X" . ,head)
                       ("X_MAIN_X" . ,main))))
      (if (null? reps)
          str
          (dbind (rep-string . rep) (car reps)
                 (loop (string-replace-substring str
                                                 rep-string
                                                 rep)
                       (cdr reps)))))))

(define (render-music-page)
  (render-base
   (generate-music-page-body)
   #:title "Music"
   #:head "<link rel=\"stylesheet\" type=\"text/css\" href=\"/static/music.css\">"))

(define (render-home-page)
  (render-base
   (generate-home-page-body)
   #:head "<link rel=\"stylesheet\" type=\"text/css\" href=\"/static/index.css\">"))

(define (render-cv-page)
  (render-base
   (generate-cv-page-body)))

(define (render-404-page)
  (render-base
   (nectar-ast->html (file->nectar-ast "templates/404.nct"))
   #:title "404"))

(define (render-fonts-page)
  (render-base
   (generate-fonts-page BUILD_DIR CACHE_DIR)
   #:title "fonts"))

(define-syntax render-nectar-with-bindings
  (syntax-rules ()
    ((_ filename bindings)
     (nectar-ast->html
      (nectar->nectar-ast (nectar-with-bindings
                           (call-with-input-file filename
                             port->nectar)
                           bindings))))))

(define (build-blog-pages-and-render)
  (let* ((post-nectar (file->nectar "templates/blog-post.nct"))
         (render-func          (lambda (post body)
                                 (render-base
                                  (nectar-ast->html
                                   (nectar->nectar-ast
                                    (nectar-with-bindings post-nectar
                                                          `((title . ,(blog-post-title post))
                                                            (date . ,(blog-post-date post))
                                                            (body . ,body)))))
                                  #:title (blog-post-plain-title post))))
         (posts
          (generate-blog-pages CONTENT_DIR BLOG_DIR CACHE_DIR
                               render-func)))
    (generate-atom "Vijay Marupudi" "https://vijaymarupudi.com"
                   (paths BLOG_DIR "atom.xml")
                   posts)
    (render-base
     (render-nectar-with-bindings "templates/blog.nct" `((blog-posts . ,posts)))
     #:title "Blog")))

(define (build)
  (system* "mkdir" "-p" BUILD_DIR)
  (system* "mkdir" "-p" BLOG_DIR)
  (system* "mkdir" "-p" CACHE_DIR)
  (let ((build-static-dir (paths BUILD_DIR STATIC_DIR)))
    (system* "mkdir" "-p" build-static-dir)
    (system* "rsync" "-aP" (string-append STATIC_DIR "/")
             (string-append build-static-dir "/")))
  (write-build-file "blog.html" (build-blog-pages-and-render))
  (write-build-file "music.html" (render-music-page))
  (write-build-file "cv.html" (render-cv-page))
  (write-build-file "index.html" (render-home-page))
  (write-build-file "fonts.html" (render-fonts-page))
  (write-build-file "404.html" (render-404-page)))

(build)



;; (display (generate-music-page-body))
;; (newline)
