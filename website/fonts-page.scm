(define-module (website fonts-page))

(use-modules (ice-9 popen)
             (ice-9 textual-ports)
             (ice-9 pretty-print)
             (srfi srfi-1)
             (srfi srfi-9)
             (ice-9 match)
             (pathlib)
             (ice-9 threads)
             (website itl-bindings)
             (ice-9 string-fun))

(define (paths . args)
  (string-join
   args
   "/"))

(define (run-command . args)
  (parameterize ((current-error-port (%make-void-port "w")))
    (let* ((port (apply open-pipe* OPEN_READ args))
           (dummy (setvbuf port 'block))
           (contents (get-string-all port)))
      (values contents (status:exit-val (close-pipe port))))))


(define-record-type font
  (make-font names paths styles)
  font?
  (names font-names)
  (paths font-paths)
  (styles font-styles))

(define-public (string-split-substring str substr)
  "Split the string @var{str} into a list of substrings delimited by the
substring @var{substr}."

  (define substrlen (string-length substr))
  (define strlen (string-length str))

  (define (loop index start)
    (cond
     ((>= start strlen) (list ""))
     ((not index) (list (substring str start)))
     (else
      (cons (substring str start index)
            (let ((new-start (+ index substrlen)))
              (loop (string-contains str substr new-start)
                    new-start))))))

  (cond
   ((string-contains str substr) => (lambda (idx) (loop idx 0)))
   (else (list str))))

(define (process-fc-list-line line)
  (let* ((items (string-split-substring line "::$::"))
         (path (list-ref items 0))
         (names (map string-trim-both (string-split (list-ref items 1) #\,)))
         (styles (let ((styles-string (list-ref items 2)))
                   ;; The other styles are translations or synonyms
                   (list (string-trim-both (car (string-split styles-string #\,)))))))
    (make-font names (list path) styles)))

(define (group-by items key ht-ref ht-set!)
  (let ((ht (make-hash-table)))
    (for-each
     (lambda (item)
       (let* ((key (key item))
              (val (ht-ref ht key)))
         (cond
          (val (ht-set! ht key (cons item val)))
          (else (ht-set! ht key (list item))))))
     items)
    (hash-map->list cons ht)))

(define (unique list)
  (let ((ht (make-hash-table)))
    (for-each
     (lambda (item)
       (hash-set! ht item #t))
     list)
    (hash-map->list (lambda (key value)
                      key) ht)))

(define (get-fonts-with-styles)
  (let* ((lines (drop-right! (string-split (run-command "fc-list" "-f" "%{file}::$::%{family}::$::%{style}\n") #\newline) 1))
         (fonts (map process-fc-list-line lines))
         (fonts (filter (lambda (font)
                          (string-prefix? (path-expanduser "/usr/share/fonts/userfonts")
                                          (car (font-paths font))))
                        fonts))
         (grouped (group-by fonts (lambda (item) (car (font-names item)))
                            hash-ref
                            hash-set!))
         (grouped-styles (map
                          (lambda (pair)
                            ;; font-name . styles
                            (cons (car pair)
                                  (let ((fonts (cdr pair)))
                                    (map
                                     (lambda (font)
                                       ;; font style name, font style paths
                                       (cons (if (null? (font-styles font))
                                                 "Regular"
                                                 (car (font-styles font)))
                                             (car (font-paths font))))
                                     fonts))))
                          grouped)))
    
    grouped-styles))

(define (fonts-to-render)
  (sort
   (map
    (lambda (item)
      (let* ((name (car item))
             (styles (cdr item))
             (style (or (find (lambda (pair)
                                (or (string=? (car pair) "Normal")
                                    (string=? (car pair) "Regular")
                                    (string=? (car pair) "Roman")
                                    (string=? (car pair) "Medium")
                                    (string=? (car pair) "Book")
                                    (string=? (car pair) "Reg")
                                    (string=? (car pair) "Med")))
                              styles)
                        (find (lambda (pair)
                                (or (string-contains (car pair) "Normal")
                                    (string-contains (car pair) "Regular")
                                    (string-contains (car pair) "Roman")
                                    (string-contains (car pair) "Medium")
                                    (string-contains (car pair) "Book")
                                    (string-contains (car pair) "Reg")
                                    (string-contains (car pair) "Med")))
                              styles)
                        (car styles))))
        ;; font-name font-path style-name 
        (vector name (cdr style) (car style))))
    (get-fonts-with-styles))
   (lambda (a b)
     (string<? (vector-ref a 0)
               (vector-ref b 0)))))

(define SENTENCE "The quick brown fox jumps over the lazy dog")



(define (generate-font-html font-info)
  (let ((dir "/static/font-samples/"))
    (string-append "<main><h1>fonts</h2><div class=\"text-content\">"
                   "<p>Here are the names and previews of fonts in my font collection. I
would recommend all of
them. Most of them are open source, but a few have been purchased.
I've made this page as a resource for those interested in fonts. If
you want to learn more about typography, I recommend <a
href=\"https://practicaltypography.com/\">Butterick's Practical
Typography</a>.</p>

<p>I have a list of proprietary fonts that I appreciate that are not
mentioned on this page. Alas, I cannot currently afford to purchase
them to automatically generate previews.</p></div>

<h2>Font list</h2>"
                   (string-concatenate
                    (map
                     (lambda (font-vector)
                       (let ((name (vector-ref font-vector 0)))
                         (string-append "<p><b>" name "</b>:<br>"
                                        (string-append "<img style=\"margin-top: 5px; width: min(800px, 100%);\"src=\""
                                                       (paths dir name) ".png" "\"" ">")
                                        "</p>\n")))
                     font-info))              
                   "</main>")))

(define (run-if-needed proc dest-path src-path)
  (when (or (not (access? dest-path F_OK))
            (let ((dest-time (stat:mtime (stat dest-path)))
                  (src-time (stat:mtime (stat src-path))))
              (> src-time dest-time)))
    (proc dest-path src-path)))

(define-public (generate-fonts-page build-dir cache-dir)
  (let* ((png-build-dir (paths build-dir "static/font-samples"))
         (font-info (fonts-to-render)))
    (system* "mkdir" "-p" png-build-dir)
    (let ((ctx (itl_font_image_renderer_ctx_new)))
      (for-each
       (lambda (font-vector)
         (let* ((name (vector-ref font-vector 0))
                (font-path (vector-ref font-vector 1))
                (sample-path (paths png-build-dir (string-append name ".png"))))
           (run-if-needed (lambda (sample-path font-path)
                            (itl-ctx-render-font-to-file ctx SENTENCE 0 50 font-path sample-path))
                          sample-path font-path)))
       font-info)
      (itl_font_image_renderer_ctx_free ctx))
    (generate-font-html font-info)))
