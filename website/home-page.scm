(define-module (website home-page))

(use-modules (nectar)
             (nectar renderers html))

(export generate-home-page-body)

(define (generate-home-page-body)
  (nectar-ast->html (nectar->nectar-ast
                     (file->nectar "templates/index.nct"))))
