(define-module (website blog-page))

(use-modules (srfi srfi-9)
             (ice-9 textual-ports)
             (ice-9 match)
             (nectar)
             (nectar tag)
             (nectar renderers html)
             (nectar renderers plain-text)
             (ice-9 ftw))

(export generate-blog-pages
        blog-post-title
        blog-post-plain-title
        blog-post-date
        blog-post-summary
        blog-post-slug)


(define-record-type <blog-post>
  (%make-blog-post title plain-title date summary slug)
  blog-post?
  (title blog-post-title)
  (plain-title blog-post-plain-title)
  (date blog-post-date)
  (summary blog-post-summary)
  (slug blog-post-slug))

(define (make-blog-post title date summary slug)
  (%make-blog-post title (nectar-ast->plain-text title) date summary slug))

(define (blog-post-serialize post)
  (match post
    (($ <blog-post> title plain-title date summary slug)
     (list title plain-title date summary slug))))

(define (blog-post-deserialize lst)
  (match lst
    ((title plain-title date summary slug)
     (%make-blog-post title plain-title date summary slug))))

(define (file->blog-post file)
  (blog-post-deserialize (call-with-input-file file
                           read)))

(define (blog-post->file post file)
  (call-with-output-file file
    (lambda (port)
      (write (blog-post-serialize post) port))))

(define (find-double-newline str)
  (string-contains str "\n\n"))

(define (extract-first-paragraph ast)

  (define (loop items ast)
    (if (null? ast)
        (reverse! items)
        (let ((head (car ast))
              (tail (cdr ast)))
          (if (string? head)
              (let ((idx (find-double-newline head)))
                (if idx
                    (reverse! (cons (substring head 0 idx) items))
                    (loop (cons head items)
                          (cdr ast))))
              (loop (cons head items)
                    (cdr ast))))))

  ;; removes leading whitespace before loop
  (if (null? ast)
      '()
      (if (string? (car ast))
          (let ((idx (string-skip (car ast) char-whitespace?)))
            (if (not idx)
                (loop '() (cdr ast))
                (loop '() (cons (substring (car ast) idx) (cdr ast)))))
          (loop '() ast))))

(define (basename path)
  (let ((idx (string-rindex path #\/)))
    (if idx
        (substring path (+ idx 1))
        path)))


(use-modules (ice-9 pretty-print))

(define (pp x)
  (pretty-print x)
  x)

(define blog-post-environment (let ((env (make-nectar-environment)))
                                (module-use! env (resolve-interface '(website nectar-env)))
                                env))


(define (nectar-debug-pretty-print ast)
  (let loop ((indent-level 0)
             (ast ast))
    (display (make-string indent-level #\space))
    (cond
     ((pair? ast) (for-each
                   (lambda (item) (loop indent-level item))
                   ast))
     ((tag? ast)
      (format #t "<~s attrs: ~s>\n" (tag-name ast) (tag-attrs ast))
      (for-each
       (lambda (item) (loop (+ indent-level 2) item))
       (tag-children ast))
      (newline))
     (else (write ast)
           (newline)))))

(define (parse-blog-post-file filename)
  (let* ((ast (call-with-input-file filename
                (lambda (port)
                  (nectar->nectar-ast (port->nectar port) blog-post-environment))))
         (title (let* ((first (begin
                                (unless (eq? (tag-name (car ast)) 'post-title)
                                  (error "Post did not contain title at top" filename))
                                (car ast))))
                  (tag-children first)))
         (body (cdr ast))
         (base (basename filename))
         (date (car (strptime "%Y-%m-%d" base)))
         (rendering (nectar-ast->html body))
         (summary (nectar-ast->html (extract-first-paragraph body))))
    (values (make-blog-post title date summary (substring base 0 (string-rindex base #\.)))
            rendering)))


(define (filename-with-extension filename ext)
  (let* ((idx (string-rindex filename #\.)))
    (if idx
        (string-append (substring filename 0 (+ idx 1)) ext)
        (string-append filename "." ext))))

(define (should-build? file-path cache-file-path)
  (not (and (file-exists? cache-file-path)
            (< (stat:mtime (stat file-path))
               (stat:mtime (stat cache-file-path))))))

(define (generate-blog-pages content-dir blog-dir cache-dir render-func)
  (let* ((blog-files (sort (map car (cddr (file-system-tree content-dir))) (compose not string<)))
         (blog-posts (map
                      (lambda (filename)
                        (let ((path (string-append content-dir "/" filename))
                              (output-path (string-append blog-dir "/"
                                                          (filename-with-extension filename "html")))
                              (cache-path (string-append cache-dir "/" filename)))
                          (if (should-build? path cache-path)
                            (call-with-values
                                (lambda () (parse-blog-post-file path))
                              (lambda (post rendering)
                                (call-with-output-file output-path
                                  (lambda (port)
                                    (put-string port (render-func post rendering))))
                                (blog-post->file post cache-path)
                                post))
                            (file->blog-post cache-path))))
                    blog-files)))
    blog-posts))

