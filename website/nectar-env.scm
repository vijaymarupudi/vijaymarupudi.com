(define-module (website nectar-env))

(use-modules (nectar)
             (guile-website-tools source-code))

(export code-block)

(define (code-block language items)
  (let ((items (if (string? items)
                   (string-trim-both items)
                   items)))
    (make-tag
     'pre
     '((class . "code-block"))
     (list
      (make-tag
       'code
       '()
       (list (cond
              ((string=? language "guile")
               (source-code->nectar-ast items))
              (else items))))))))


