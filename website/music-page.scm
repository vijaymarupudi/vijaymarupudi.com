(define-module (website music-page))

(use-modules (sqlite3)
             (ice-9 match)
             (ice-9 receive)
             (website utils)
             (nectar)
             (nectar renderers html))


(define db-path (string-append HOME "/techno/linux/musiclibrary.db"))

(define (simple-query db query)
  (let* ((stmt (sqlite-prepare db query))
         (rows (let loop ((rows '()))
                 (let ((row (sqlite-step stmt)))
                   (if row
                       (loop (cons (vector->list row) rows))
                       rows)))))
    
    (sqlite-finalize stmt)
    (reverse! rows)))

(define (get-tracks db)
  (simple-query db "SELECT artist, title FROM items WHERE album = '' ORDER BY artist;"))

(define (get-albums db)
  (simple-query db "SELECT albumartist, album FROM albums ORDER BY albumartist;"))

(define (get-tracks-and-albums)
  (let* ((db (sqlite-open db-path))
         (tracks (get-tracks db))
         (albums (get-albums db))
         (last-modified-time (stat:mtime (stat db-path)))
         (last-modified-time (strftime "%Y%m%dT%H%M%S" (localtime last-modified-time))))
    (sqlite-close db)
    (values tracks albums last-modified-time)))

(define (generate-music-page-body)
  (receive (tracks albums last-modified-time) (get-tracks-and-albums)
    (let* ((nectar (call-with-input-file "templates/music-page.nct"
                     port->nectar))
           (nectar  (nectar-with-bindings nectar
                                          `((inp-tracks . ,tracks)
                                            (inp-albums . ,albums)
                                            (last-modified-time . ,last-modified-time))))
           (ast (nectar->nectar-ast nectar)))
      (nectar-ast->html ast))))

(export generate-music-page-body)
