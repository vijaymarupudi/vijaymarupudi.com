(define-module (website atom)
  #:use-module (website blog-page)
  #:use-module (sxml simple))

(export generate-atom)

(define (date->atom-date date)
  (strftime "%Y-%m-%dT%H:%M:%SZ" date))

(define (paths . args)
  (string-join
   args
   "/"))

(define (slug->url base slug)
  (paths base "blog" (string-append slug ".html")))

(define (generate-atom title url filepath blog-posts)
  (when (char=? (string-ref url (- (string-length url) 1)) #\/)
    (error "Don't pass a url with a trailing slash"))
  (let* ((entries (map
                   (lambda (post)
                     `(entry
                       (title ,(blog-post-plain-title post))
                       (published ,(date->atom-date (blog-post-date post)))
                       (updated ,(date->atom-date (blog-post-date post)))
                       (content (@ (type "html")) ,(blog-post-summary post))
                       (id ,(slug->url url (blog-post-slug post)))
                       (link (@ (href ,(slug->url url (blog-post-slug post)))))
                       (author (name "Vijay Marupudi"))))
                   blog-posts))
         (sxml `(feed (@ (xmlns "http://www.w3.org/2005/Atom"))
                      (id ,(string-append url "/"))
                      (title ,title)
                      (link (@ (rel "self")
                               (href ,(paths url "blog" "atom.xml"))))
                      (updated ,(date->atom-date (blog-post-date (car blog-posts))))
                      . ,entries)))
    (call-with-output-file filepath
      (λ (port)
        (sxml->xml sxml port)))))
