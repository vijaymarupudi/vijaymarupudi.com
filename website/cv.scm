(define-module (website cv))

(use-modules (nectar)
             (nectar renderers html)
             (website utils)
             (vj-cv)
             (ice-9 receive)
             (srfi srfi-1)
             (ice-9 match))

(export generate-cv-page-body)

(define (render-rich-text->html rt)
  (define (render-item item acc)
    (match item
      ((? string? x) (cons (nectar-html-escape-string item) acc))
      ((? number? x) (cons (number->string x) acc))
      (('em item) (cons "</em>" (render-item item (cons "<em>" acc))))
      (('url dest item) (cons "</a>" (render-item item
                                                  (cons
                                                   (string-append "<a " "href=\"" (nectar-html-escape-string dest) "\"" ">")
                                                   acc))))
      (('url dest)
       (cons "</a>" (render-item dest
                                                  (cons
                                                   (string-append "<a " "href=\"" (nectar-html-escape-string dest) "\"" ">")
                                                   acc))))
      (('b x)
       (cons "</b>" (render-item x (cons "<b>" acc))))
      (('list . items)
       (fold render-item acc items))))

  (string-concatenate
   (reverse!
    (render-item rt '()))))

(define (generate-cv-page-body)
  (receive (journals proceedings posters-and-talks) (portfolio-apa-citations)
    (let ((journal-cites (map render-rich-text->html journals))
          (proceedings-cites (map render-rich-text->html proceedings))
          (posters-and-talks-cites (map render-rich-text->html posters-and-talks))
          (awards (portfolio-awards))
          (education (portfolio-education))
          (research (portfolio-research))
          (work (portfolio-work))
          (teaching (portfolio-teaching)))
      (nectar-ast->html (nectar->nectar-ast
                         (nectar-with-bindings
                          (file->nectar "templates/cv.nct")
                          `((journal-cites . ,journal-cites)
                            (proceedings-cites . ,proceedings-cites)
                            (posters-and-talks-cites . ,posters-and-talks-cites)
                            (awards . ,awards)
                            (education . ,education)
                            (research . ,research)
                            (work . ,work)
                            (teaching . ,teaching))))))))
