(define-module (website itl-bindings))

(use-modules (ice-9 pretty-print)
             (srfi srfi-1)
             (system foreign)
             (rnrs bytevectors))

(define itl (dynamic-link "libitl"))

(define-public itl_font_image_renderer_ctx_new (pointer->procedure '*
                                                            (dynamic-func "itl_font_image_renderer_ctx_new" itl) (list)))



(define-public itl_font_image_renderer_ctx_free (pointer->procedure void (dynamic-func "itl_font_image_renderer_ctx_free" itl) (list '*)))


(define itl_render_font_image_to_file (pointer->procedure void (dynamic-func "itl_render_font_image_to_file" itl) (list '* '* '* '* size_t uint32 uint32)))


(define-public (itl-ctx-render-font-to-file ctx text margin font-size font-file-path image-file-path)
  (let ((strbv (string->utf8 text)))
    (itl_render_font_image_to_file ctx (string->pointer font-file-path)
                                   (string->pointer image-file-path)
                                   (bytevector->pointer strbv)
                                   (bytevector-length strbv)
                                   margin
                                   font-size)))




