.PHONY: build deploy

build:
	guile -L . -s main.scm

serve:
	cd build/ && python -m http.server

deploy: build
	rsync build/ server:/web/vijaymarupudi.com/ -aP

clean:
	rm -r build cache
